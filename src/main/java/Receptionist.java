import javax.inject.Singleton;

@Singleton
public class Receptionist {
    public void hello(String s) {
        System.out.println(s);
    }
}
