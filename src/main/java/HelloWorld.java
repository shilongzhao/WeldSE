import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class HelloWorld {
    @PostConstruct
    void init() {
        System.out.println("Initializing Application ... ");
    }

    void printHello(@Observes ContainerInitialized event,
                    @Parameters List<String> params, Receptionist worker) {
        worker.hello("Hello " + params.get(0));
    }
}
